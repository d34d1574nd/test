const { I } = inject();
// Add in your custom step files


Given('Я нахожусь на главной странице', () => {
  I.amOnPage('/')
});

When('Я нажимаю на кнопку модального окна', () => {
  I.waitForVisible({xpath: `//i[@class='fas fa-envelope-open-text ModalButton-envelope']`});
  I.click({xpath: `//i[@class='fas fa-envelope-open-text ModalButton-envelope']`});
});

When('Я ввожу все нужные данные в форму для связи и нажимаю отправить', () => {
  I.waitForVisible({xpath: `//h5[.='Форма для связи']`});
  I.fillField({xpath:`//input[@id='name']`}, "Илья Нестеров");
  I.fillField({xpath:`//input[@id='phone']`}, "(555) 55-55-55");
  I.fillField({xpath:`//input[@id='email']`}, "d34d1574nd@mail.ru");
  I.fillField({xpath:`//textarea[@id='message']`}, "Срочно свяжитесь со мной, мне нжуны Ваши услуги");
  I.click({xpath: `//button[.='Отправить']`});
});

Then('Я вижу сообщение {string}', message => {
  I.waitForText(message);
});

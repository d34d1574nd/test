const { I } = inject();
// Add in your custom step files


Given('Я нахожусь на главной странице', () => {
  I.amOnPage('/')
});

When('Я вижу логотип, ссылку на почту, ссылку на телефон, ссылку на карту', () => {
  I.waitForVisible({xpath: `//a[@class='logo nav-link active']`});
  I.waitForVisible({xpath: `//span[contains(text(),'ilyanesterov17@gmail.com')]`});
  I.waitForVisible({xpath: `//span[contains(text(),'0755886434')]`});
  I.waitForVisible({xpath: `//span[contains(text(),'10-6')]`});
});

When('Я вижу меню навигации, ссылку на услуги, ссылку на отзывы, ссылку о себе', () => {
  I.waitForVisible({xpath: `//ul[@class='nav']`});
  I.waitForVisible({xpath: `//div[@class='header-navigation--nav']//a[.='Услуги']`});
  I.waitForVisible({xpath: `//div[@class='header-navigation--nav']//a[.='О Себе']`});
  I.waitForVisible({xpath: `//div[@class='header-navigation--nav']//a[.='Отзывы']`});
});


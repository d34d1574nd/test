const { I } = inject();
// Add in your custom step files


Given('Я нахожусь на главной странице', () => {
  I.amOnPage('/')
});

When('Я переключаю меню навигации на отзывы', () => {
  I.click({xpath: `//div[@class='header-navigation--nav']//a[.='Отзывы']`});
  I.waitForVisible({xpath: `//div[@class='container']//h2[.='Отзывы']`});
});

Then('Я вижу каждый блок из страницы отзывов', () => {
  I.waitForVisible({xpath: `//div[@class='reviews_column column-1 col-sm-6']`});
  I.waitForVisible({xpath: `//div[@class='reviews_column column-1 col-sm-6']//p[@class='reviews_users'][contains(text(),'Lorem Ipsum is simply dummy text of the printing a')]`});
  I.waitForVisible({xpath: `//div[@class='reviews_column column-2 col-sm-6']`});
  I.waitForVisible({xpath: `//div[@class='reviews_column column-1 col-sm-6']//p[@class='reviews_users'][contains(text(),'Lorem Ipsum is simply dummy text of the printing a')]`});
  I.waitForVisible({xpath: `//div[@class='reviews_column column-3 col-sm-6']`});
  I.waitForVisible({xpath: `//div[@class='reviews_column column-1 col-sm-6']//p[@class='reviews_users'][contains(text(),'Lorem Ipsum is simply dummy text of the printing a')]`});
  I.waitForVisible({xpath: `//div[@class='reviews_column column-4 col-sm-6']`});
  I.waitForVisible({xpath: `//div[@class='reviews_column column-1 col-sm-6']//p[@class='reviews_users'][contains(text(),'Lorem Ipsum is simply dummy text of the printing a')]`});
});

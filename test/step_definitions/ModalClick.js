const { I } = inject();
// Add in your custom step files


Given('Я нахожусь на главной странице', () => {
  I.amOnPage('/')
});

When('Я нажимаю на кнопку модального окна', () => {
  I.waitForVisible({xpath: `//i[@class='fas fa-envelope-open-text ModalButton-envelope']`});
  I.click({xpath: `//i[@class='fas fa-envelope-open-text ModalButton-envelope']`});
});

When('Я вижу модальное окно, закрываю его', () => {
  I.waitForVisible({xpath: `//h5[.='Форма для связи']`});
  I.click({xpath: `//button[.='Закрыть']`});
});

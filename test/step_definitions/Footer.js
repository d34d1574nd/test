const { I } = inject();
// Add in your custom step files

Given('Я нахожусь на главной странице', () => {
  I.amOnPage('/')
});

When('Я переключаю кнопки в подвале', () => {
  I.click({xpath: `//div[@class='header-navigation--nav']//a[.='Услуги']`});
  I.waitForVisible({xpath: `//div[@class='container']//h2[.='Услуги']`});
  I.waitForVisible({xpath: `//a[@class='logoWhite nav-link']//img[@class='header-logo__image']`});
  I.click({xpath: `//a[@class='logoWhite nav-link']//img[@class='header-logo__image']`});
  I.click({xpath: `//a[@class='buttonInstagram telegramim_shadow telegramim_pulse']`});
  I.click({xpath: `//a[@class='telegramim_button telegramim_shadow telegramim_pulse buttonTelegram']`});
});

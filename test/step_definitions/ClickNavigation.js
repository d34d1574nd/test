const { I } = inject();
// Add in your custom step files


Given('Я нахожусь на главной странице', () => {
  I.amOnPage('/')
});

When('Я переключаю меню навигации и вижу каждую страницу', () => {
  I.click({xpath: `//div[@class='header-navigation--nav']//a[.='Услуги']`});
  I.waitForVisible({xpath: `//div[@class='container']//h2[.='Услуги']`});

  I.click({xpath: `//div[@class='header-navigation--nav']//a[.='О Себе']`});
  I.waitForElement({xpath: `//div[@class='container']//h2[.='Обо мне']`});

  I.click({xpath: `//div[@class='header-navigation--nav']//a[.='Отзывы']`});
  I.waitForElement({xpath: `//div[@class='container']//h2[.='Отзывы']`});
});

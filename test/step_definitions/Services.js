const { I } = inject();
// Add in your custom step files


Given('Я нахожусь на главной странице', () => {
  I.amOnPage('/')
});

When('Я переключаю меню навигации на услугу', () => {
  I.click({xpath: `//div[@class='header-navigation--nav']//a[.='Услуги']`});
  I.waitForVisible({xpath: `//div[@class='container']//h2[.='Услуги']`});
});

Then('Я вижу каждый блок из страницы услуг', () => {
  I.waitForVisible({xpath: `//h6[@class='lamp']`});
  I.waitForVisible({xpath: `//h6[@class='tasks']`});
  I.waitForVisible({xpath: `//h6[@class='cogs']`});
  I.waitForVisible({xpath: `//h6[@class='mobile']`});
});

const { I } = inject();
// Add in your custom step files


Given('Я нахожусь на главной странице', () => {
  I.amOnPage('/')
});

When('Я должен увидеть карусель', () => {
  I.waitForVisible({xpath: `//div[@class='carousel carousel-slider']`});
});

When('Я пробую переключить слайдер', () => {
  I.dragSlider({xpath: `//div[@class='carousel carousel-slider']`}, 30);
  I.dragSlider({xpath: `//div[@class='carousel carousel-slider']`}, -70);
});

const { I } = inject();
// Add in your custom step files


Given('Я нахожусь на главной странице', () => {
  I.amOnPage('/')
});

When('Я нажимаю на логотип, на ссылку на почту, и вижу почту, вернувшись на главный экран я нажимаю на ссылку на телефон, на ссылку на карту', () => {
  I.click({xpath: `//a[@class='logo nav-link active']`});
  I.click({xpath: `//span[contains(text(),'ilyanesterov17@gmail.com')]`});
  I.click({xpath: `//span[contains(text(),'0755886434')]`});
  I.click({xpath: `//span[contains(text(),'10-6')]`});
});

When('Я вижу меню навигации, и нажимаю на ссылку на услуги, на ссылку на отзывы, на ссылку о себе', () => {
  I.waitForVisible({xpath: `//ul[@class='nav']`});
  I.waitForVisible({xpath: `//div[@class='header-navigation--nav']//a[.='Услуги']`});
  I.waitForVisible({xpath: `//div[@class='header-navigation--nav']//a[.='О Себе']`});
  I.waitForVisible({xpath: `//div[@class='header-navigation--nav']//a[.='Отзывы']`});
});

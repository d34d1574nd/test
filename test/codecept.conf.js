const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// HEADLESS=true npx codecept run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './*_test.js',
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://localhost:3000',
      show: true,
      windowSize: '1200x900'
    }
  },
  include: {
    I: './steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'test',
  translation: 'ru-RU',
  gherkin: {
    features: './features/*.feature',
    steps: [
      './step_definitions/Footer.js',
      './step_definitions/Toolbar.js',
      './step_definitions/ToolbarClick.js',
      './step_definitions/CarouselSlider.js',
      './step_definitions/ModalClick.js',
      './step_definitions/SendMessageFromModal.js',
      './step_definitions/ClickNavigation.js',
      './step_definitions/Services.js',
      './step_definitions/Reviews.js',
    ]
  },
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}

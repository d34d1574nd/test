import React from 'react';
import PropTypes from 'prop-types';
import {Col, FormGroup, Input} from "reactstrap";

const FormElement = ({propertyName, title, error, mask, valid, onClick, ...props}) => {
  return (
    <FormGroup row>
      <Col sm={12}>
          <Input
            name={propertyName} id={propertyName}
            invalid={!!error} valid={valid}
            mask={mask} onClick={onClick}
            {...props}
          />
      </Col>
    </FormGroup>
  );
};

FormElement.propTypes = {
  propertyName: PropTypes.string.isRequired,
  title: PropTypes.string,
  mask: PropTypes.string,
  valid: PropTypes.bool,
  error: PropTypes.string,
};

export default FormElement;

import React from 'react';
import PropTypes from 'prop-types';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

import "./ModalWindow.css";


const ModalWindow = props => {
  return (
    <Modal isOpen={props.modal} toggle={props.toggleModal} className="ModalWindow">
      <ModalHeader>Форма для связи</ModalHeader>
      <ModalBody>
        {props.children}
      </ModalBody>
      <ModalFooter>
        <Button color="danger" onClick={props.toggleModal}>Закрыть</Button>
        <Button color="primary" onClick={props.send}>Отправить</Button>
      </ModalFooter>
    </Modal>
  )
    ;
};

ModalWindow.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  send: PropTypes.func.isRequired,
  modal: PropTypes.bool
};

export default ModalWindow;

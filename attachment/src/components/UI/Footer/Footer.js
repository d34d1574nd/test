import React from 'react';
import {Col, Container, NavLink, Row} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import logo from "../../../assets/logo-white.png";

import "./Footer.css";

const Footer = () => {
  return (
    <footer>
      <Container>
        <Row>
          <Col>
            <NavLink tag={RouterNavLink} to="/" exact className="logoWhite">
              <img src={logo} alt="Thunder" className="header-logo__image"/>
              Thunder
            </NavLink>
          </Col>
          <Col>
            <a
              href="https://www.instagram.com/ilya_island/?hl=ru" target="_blank" rel="noopener noreferrer"
              className="buttonInstagram telegramim_shadow telegramim_pulse"><i className="ftelegramim instagram_icon"/> Instagram</a>
          </Col>
          <Col>

            <a href="https://telegram.im/@d34d1574nd" target="_blank" rel="noopener noreferrer"
               className="telegramim_button telegramim_shadow telegramim_pulse buttonTelegram"
               title=""><i/> Telegram</a>

          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;

import React, {Fragment} from 'react';
import {Container, Nav, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import "./MenuLogo.css";

const MenuLogo = ({logo}) => {
  return (
    <Fragment>
      <Container>
        <Nav className="header_navigation--logo">
          <NavLink tag={RouterNavLink} to="/" exact className="logo">
            <img src={logo} alt="Thunder" className="header-logo__image"/>
            Thunder
          </NavLink>
          <a href="mailto:ilyanesterov17@gmail.com" target="_blank" rel="noopener noreferrer" className="header-logo__links">
            <i className="fas fa-envelope-open-text header-envelope"/>
            <span>ilyanesterov17@gmail.com</span>
          </a>
          <a href="tel:+996755886434" className="header-logo__links">
            <i className="fas fa-mobile-alt header-envelope"/>
            <span>0755886434</span>
          </a>
          <a href="geo:42.877496,74.690877"  className="header-logo__links">
            <i className="fas fa-map-marked-alt header-envelope"/>
            <span>гор. Энергетиков 10-6</span>
          </a>
        </Nav>
      </Container>
    </Fragment>
  );
};

export default MenuLogo;

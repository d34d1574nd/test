import React from 'react';
import {Container, Nav, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import "./MenuNav.css";

const MenuNav = () => {
  return (
      <div className="header-navigation--nav">
        <Container>
          <Nav>
            <NavLink to="/services" tag={RouterNavLink} exact className="navigation_nav--links">Услуги</NavLink>
            <NavLink to="/about_us" tag={RouterNavLink} exact className="navigation_nav--links">О Себе</NavLink>
            <NavLink to="/reviews" tag={RouterNavLink} exact className="navigation_nav--links">Отзывы</NavLink>
          </Nav>
        </Container>
      </div>
  );
};

export default MenuNav;

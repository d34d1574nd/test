import React, {Fragment} from 'react';

import MenuLogo from "./Menu/MenuLogo/MenuLogo";
import MenuNav from "./Menu/MenuNav/MenuNav";

import Logo from "../../../assets/logo.jpg";


const Toolbar = () => {
    return (
        <Fragment>
          <MenuLogo logo={Logo}/>
          <MenuNav/>
        </Fragment>
    );
};

export default Toolbar;

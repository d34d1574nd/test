import React, {Fragment} from 'react';
import { Carousel } from 'react-responsive-carousel';

import Html from "../../assets/html.jpg";
import Css from "../../assets/css.jpg";
import Js from "../../assets/javascript.png";

import "./CarouselComponent.css";

const CarouselComponent = () => {
  return (
    <Fragment>
      <Carousel autoPlay showArrows={false}  showThumbs={false} emulateTouch infiniteLoop dynamicHeight={false} interval={3000} swipeable className="attachment-carousel">
        <div>
          <img src={Html} alt="html code" />
        </div>
        <div>
          <img src={Css} alt="css code" />
        </div>
        <div>
          <img src={Js} alt="javascript code" />
        </div>
      </Carousel>
    </Fragment>
  );
};

export default CarouselComponent;

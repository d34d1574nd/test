import React from 'react';
import {Card, CardBody, CardImg, CardText, Col, Container, Row} from "reactstrap";

import ReactImage from '../../assets/react.png';
import NodeImage from '../../assets/node.jpeg';
import MongoImage from '../../assets/mongo.png';

import "./Tongue.css";


const Tongue = () => {
  return (
    <Container>
      <h2 className="tongue">Языки которыми я владею</h2>
      <Row>
        <Col sm="4">
          <a href="https://ru.wikipedia.org/wiki/React" target="_blank" rel="noopener noreferrer" className="card-link">
            <Card>
              <CardImg top width="100%" src={ReactImage} alt="React js" />
              <CardBody>
                <CardText>
                  React (иногда React.js или ReactJS) — JavaScript-библиотека с открытым исходным кодом для разработки пользовательских интерфейсов.
                  React разрабатывается и поддерживается Facebook, Instagram и сообществом отдельных разработчиков и корпораций
                  <span>...</span>
                </CardText>
              </CardBody>
            </Card>
          </a>
        </Col>
        <Col sm="4">
          <a href="https://ru.wikipedia.org/wiki/Node.js" target="_blank" rel="noopener noreferrer" className="card-link">
            <Card>
              <CardImg top width="100%" src={NodeImage} alt="Node js" />
              <CardBody>
                <CardText>
                  Node или Node.js — программная платформа, основанная на движке V8 (транслирующем JavaScript в машинный код),
                  превращающая JavaScript из узкоспециализированного языка в язык общего назначения.
                  Node.js добавляет возможность JavaScript взаимодействовать с устройствами ввода-вывода через свой API (написанный на C++),
                  подключать другие внешние библиотеки, написанные на разных языках, обеспечивая вызовы к ним из JavaScript-кода.
                  Node.js применяется преимущественно на сервере, выполняя роль веб-сервера,
                  но есть возможность разрабатывать на Node.js и десктопные оконные приложения (при помощи NW.js,
                  AppJS или Electron для Linux, Windows и macOS) и даже программировать микроконтроллеры (например, tessel, low.js и espruino).
                  В основе Node.js лежит событийно-ориентированное и асинхронное (или реактивное) программирование с неблокирующим вводом/выводом.
                  <span>...</span>
                </CardText>
              </CardBody>
            </Card>
          </a>
        </Col>
        <Col sm="4">
          <a href="https://ru.wikipedia.org/wiki/MongoDB" target="_blank" rel="noopener noreferrer" className="card-link">
            <Card>
              <CardImg top width="100%" src={MongoImage} alt="Mongo Db" />
              <CardBody>
                <CardText>
                  MongoDB — документоориентированная система управления базами данных с открытым исходным кодом,
                  не требующая описания схемы таблиц. Классифицирована как NoSQL, использует JSON-подобные документы и схему базы данных.
                  Написана на языке C++. Используется в веб-разработке, в частности, в рамках JavaScript-ориентированного стека MEAN.
                  <span>...</span>
                </CardText>
              </CardBody>
            </Card>
          </a>
        </Col>
      </Row>
    </Container>
  );
};

export default Tongue;

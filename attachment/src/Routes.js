import React from 'react';
import {Route, Switch} from "react-router-dom";

import Attachment from "./containers/Attachment/Attachment";
import Reviews from "./containers/Reviews/Reviews";
import Services from "./containers/Services/Services";
import AboutUs from "./containers/AboutUs/AboutUs";

const Routes = () => {
    return (
        <Switch>
             <Route path="/" exact component={Attachment}/>
             <Route path="/reviews" exact component={Reviews}/>
             <Route path="/services" exact component={Services}/>
             <Route path="/about_us" exact component={AboutUs}/>
        </Switch>
    );
};

export default Routes;

import React, {Component, Fragment} from 'react';
import {NotificationContainer} from "react-notifications";

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Footer from "./components/UI/Footer/Footer";
import Routes from "./Routes";

import './App.css';


class App extends Component {
  render() {
    return (
        <Fragment>
          <NotificationContainer/>
          <Toolbar/>
          <Routes/>
          <Footer/>
        </Fragment>
    );
  }
}

export default App;

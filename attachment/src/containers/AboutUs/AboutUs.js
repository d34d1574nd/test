import React, {Component} from 'react';
import {Container} from "reactstrap";

import Diploma from "../../assets/diploma.png";

import "./AboutUs.css";

class AboutUs extends Component {
  render() {
    return (
      <div className="about_us">
        <Container>
          <h2>Обо мне</h2>
          <p className="about_us_text">Вы находитесь на моём персональном сайте-портфолио. Ниже вы найдёте много информации обо мне, моём опыте, навыках и проектах. Надеюсь, будет интересно :-)</p>

          <p className="about_us_name">Нестеров Илья Александрович</p>
          <p className="about_us_profession">FullStack-разработчик</p>
          <p><strong>Образование:</strong> среднее-специально. Attractor-School.</p>

          <p><strong>Сертификаты:</strong></p>
          <a href="http://attractor-school.com/bishkek" target="_blank" rel="noopener noreferrer">
            <img src={Diploma} alt="diploma"/>
          </a>


          <p><strong>Специализация:</strong> Программирование сайтов-визиток / корпоративных сайтов / интернет магазинов / сайтов под ключ / Web-сервисов. Адаптивная вёрстка.</p>
          <p><strong>Опыт работы:</strong> более 1,5 лет.</p>
          <p><strong>Используемые технологии:</strong> HTML, CSS (SASS), JavaScript (jQuery), ReactJS, NodeJS, MySQL, MongoDB, PostgreSQL.</p>
        </Container>

      </div>
    );
  }
}

export default AboutUs;

import React, {Component} from 'react';
import {Col, Container, Row} from "reactstrap";


import "./Reviews.css";


class Reviews extends Component {
  render() {
    return (
      <div className="reviews">
        <Container>
          <h2>Отзывы</h2>
            <Row>
              <Col sm="6" className="reviews_column column-1">
                <p className="reviews_users">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.
                </p>
                <p><span className="reviews_users-name">Zam Cristafr </span></p>
              </Col>
              <Col sm="6" className="reviews_column column-2">
                <p className="reviews_users">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.
              </p>
                <p><span className="reviews_users-name">Darwin Michle </span></p>
              </Col>
              <Col sm="6" className="reviews_column column-3">
                <p className="reviews_users">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.
              </p>
                <p><span className="reviews_users-name">Clips Arter </span></p>
              </Col>
              <Col sm="6" className="reviews_column column-4">
                <p className="reviews_users">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.
              </p>
                <p><span className="reviews_users-name">John Doe </span></p>
              </Col>
            </Row>
        </Container>
      </div>
    );
  }
}

export default Reviews;

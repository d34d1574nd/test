import React, {Component} from 'react';
import {Button, Form} from "reactstrap";
import {NotificationManager} from 'react-notifications';
import InputMask from 'react-input-mask';

import CarouselComponent from "../../components/CarouselComponent/CarouselComponent";
import ModalWindow from "../../components/UI/ModalWindow/ModalWindow";
import FormElement from "../../components/Form/FormElement";
import Tongue from "../../components/Tongue/Tongue";

import "./Attachment.css";


class Attachment extends Component {
  state = {
    modal: false,
    name: '',
    email: '',
    phone: '',
    message: ''
  };

  showModal = () => {
    this.setState({modal: true})
  };

  hideModal = () => {
    this.setState({modal: false})
  };


  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();
    NotificationManager.success('Сообщение успешно отправлено!');
    this.setState({modal: false})
  };


    render() {
        return (
            <div className="attachment">
                <CarouselComponent/>
                <Button className="ModalButton"  onClick={() => this.showModal()} color="primary">Свяжитесь с нами <i className="fas fa-envelope-open-text ModalButton-envelope"/></Button>
              <ModalWindow isOpen={this.state.modal} toggleModal={this.hideModal} modal={this.state.modal} send={this.submitFormHandler}>
                <Form onSubmit={this.submitFormHandler}>
                  <FormElement
                    propertyName="name"
                    type="text"
                    valid={this.state.name !== ''}
                    value={this.state.name}
                    onChange={this.inputChangeHandler}
                    placeholder="Имя"
                  />
                  <FormElement
                    propertyName="phone"
                    type="tel"
                    value={this.state.phone}
                    mask="+\9\96 (999) 99-99-99"
                    maskChar="_"
                    tag={InputMask}
                    onChange={this.inputChangeHandler}
                    placeholder="Номер телефона"
                  />
                  <FormElement
                    propertyName="email"
                    type="email"
                    valid={this.state.email !== ''}
                    value={this.state.email}
                    onChange={this.inputChangeHandler}
                    placeholder="E-mail - email@example.com"
                  />
                  <FormElement
                    propertyName="message"
                    type="textarea"
                    valid={this.state.message !== ''}
                    value={this.state.message}
                    onChange={this.inputChangeHandler}
                    placeholder="Сообщение"
                  />
                </Form>
              </ModalWindow>
              <Tongue/>
            </div>
        );
    }
}

export default Attachment;

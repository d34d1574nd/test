import React, {Component} from 'react';
import {Col, Container, Row} from "reactstrap";

import "./Services.css";

class Services extends Component {
  render() {
    return (
      <div className="services">
        <Container>
          <h2>Услуги</h2>
          <p> Ниже можно найти список работ с которыми я могу вам помочь. Если вам необходимо реализовать что-либо из списка - свяжитесь со мной!</p>
          <Row className="services_columns">
            <Col sm="3">
              <i className="fas fa-lightbulb icon"/>
              <h6 className="lamp">Консультация и аналитика</h6>
              <span>Консультации на любом этапе разработки Web-проекта. Профессионально. Бесплатно.</span>
            </Col>
            <Col sm="3">
              <i className="fa fa-tasks icon"/>
              <h6 className="tasks">Техническое задание</h6>
              <span>Разработка технического задания и спецификации Web-проекта. Проповедую SCRUM, где это целесообразно.</span>
            </Col>
            <Col sm="3">
              <i className="fa fa-cogs icon"/>
              <h6 className="cogs">Разработка проекта</h6>
              <span>Программирование web-проектов от визитки до web-сервисов.</span>
            </Col>
            <Col sm="3">
              <i className="fas fa-mobile-alt icon"/>
              <h6 className="mobile">Мобильные версии</h6>
              <span>Разработка адаптивных и мобильных сайтов - самое актуальное в эру смартфонов и планшетов.</span>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Services;
